package ru.rencredit.jschool.kuzyushin.tm.api.service;

public interface IPropertyService {

    String getServerHost();

    Integer getServerPort();

    String getSessionSalt();

    Integer getSessionCycle();

    String getJdbcDriver();

    String getJdbcUrl();

    String getJdbcUsername();

    String getJdbcPassword();
}
