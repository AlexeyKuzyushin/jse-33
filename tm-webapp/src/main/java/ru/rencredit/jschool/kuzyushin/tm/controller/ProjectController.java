package ru.rencredit.jschool.kuzyushin.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @Autowired
    public ProjectController(
            final @NotNull IUserService userService,
            final @NotNull IProjectService projectService
    ) {
        this.userService = userService;
        this.projectService = projectService;
    }

    @GetMapping("")
    public ModelAndView show() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("/project/project-list");
        modelAndView.addObject("projects", projectService.findAll());
        return modelAndView;
    }

    @GetMapping("/create")
    public ModelAndView create() {
        return new ModelAndView("/project/project-create");
    }

    @PostMapping("/create")
    public ModelAndView create(
            final @NotNull Project project,
            @RequestParam(value = "userId") final @NotNull  String userId
    ) {
        projectService.create(userId, project.getName(), project.getDescription());
        return new ModelAndView("redirect:/projects");
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable(value = "id") final @NotNull String id) {
        projectService.removeProjectById(id);
        return "redirect:/projects";
    }

    @GetMapping("/update/{id}")
    public ModelAndView update(@PathVariable(value = "id") final @NotNull String id) {
        @Nullable final Project project = projectService.findProjectById(id);
        return new ModelAndView("/project/project-update", "project", project);
    }

    @PostMapping("/update/{id}")
    public String update(@ModelAttribute("project") final @NotNull Project project) {
        projectService.updateProjectById(project.getId(),
                project.getName(), project.getDescription());
        return "redirect:/projects";
    }

    @GetMapping("/view/{id}")
    public ModelAndView view(@PathVariable("id") @NotNull final String id) {
        @Nullable final Project project = projectService.findProjectById(id);
        return new ModelAndView("/project/project-view", "project", project);
    }
}
