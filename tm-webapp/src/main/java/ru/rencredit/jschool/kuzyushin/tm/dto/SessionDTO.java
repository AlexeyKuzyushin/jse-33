package ru.rencredit.jschool.kuzyushin.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.AbstractEntity;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractEntityDTO implements Cloneable{

    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    @Nullable
    private String userId;

    @Nullable
    private Long timestamp;

    @Nullable
    private String signature;

    @Nullable
    public static SessionDTO toDTO(@Nullable final Session session) {
        if (session == null) return null;
        return new SessionDTO(session);
    }

    @NotNull
    public static List<SessionDTO> toDTO(@Nullable final List<Session> sessions) {
        if (sessions == null || sessions.isEmpty()) return Collections.emptyList();
        @NotNull final List<SessionDTO> result = new ArrayList<>();
        for (@Nullable final Session session: sessions) {
            if (session == null) continue;
            result.add(new SessionDTO(session));
        }
        return result;
    }

    public SessionDTO(@Nullable final Session session) {
        if (session == null) return;
        id = session.getId();
        timestamp = session.getTimestamp();
        signature = session.getSignature();
        if (session.getUser() != null) userId = session.getUser().getId();
    }
}
