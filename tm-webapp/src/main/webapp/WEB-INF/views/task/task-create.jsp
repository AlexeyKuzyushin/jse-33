<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/_header.jsp" />

<h1>TASK CREATE</h1>

<form action="/tasks/create" method="POST">
    <p>
        <div>USER-ID</div>
        <div><input type="text" name="userId" value="${userId}"></div>
    </p>
    <p>
        <div>PROJECT-ID</div>
        <div><input type="text" name="projectId" value="${projectId}"></div>
    </p>
    <p>
        <div>NAME</div>
        <div><input type="text" name="name" value="${task.name}"></div>
    </p>
    <p>
        <div>DESCRIPTION</div>
        <div><input type="text" name="description" value="${task.description}"></div>
    </p>
    <button type="submit">SAVE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp" />