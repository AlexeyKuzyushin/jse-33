<jsp:include page="../include/_header.jsp" />

<h1>PROJECT UPDATE</h1>

<form action="/projects/update/${project.id}" method="POST">
    <p>
        <div>NAME:</div>
        <div><input type="text" name="name" value="${project.name}"></div>
    </p>
    <p>
        <div>DESCRIPTION:</div>
        <div><input type="text" name="description" value="${project.description}"></div>
    </p>
    <button type="submit">SAVE PROJECT</button>
</form:form>

<jsp:include page="../include/_footer.jsp" />