package ru.rencredit.jschool.kuzyushin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskEndpoint;

@Component
public final class TaskCountListener extends AbstractListener {

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @Autowired
    public TaskCountListener(
            final @NotNull TaskEndpoint taskEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.taskEndpoint = taskEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "task-count";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Count all tasks";
    }

    @Override
    @EventListener(condition = "@taskCountListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COUNT TASKS]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        System.out.println("COUNT: " + taskEndpoint.countAllTasks(sessionDTO));
        System.out.println("[OK]");
    }
}
