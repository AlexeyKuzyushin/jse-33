package ru.rencredit.jschool.kuzyushin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.Exception_Exception;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserEndpoint;

@Component
public final class UserViewProfileListener extends AbstractListener {

    @NotNull
    private final UserEndpoint userEndpoint;

    @Autowired
    public UserViewProfileListener(
            final @NotNull UserEndpoint userEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.userEndpoint = userEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW USER PROFILE]");
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @Nullable final UserDTO userDTO = userEndpoint.viewUserProfile(sessionDTO);
        if (userDTO == null) return;
        System.out.println("ID: " + userDTO.getId());
        System.out.println("LOGIN: " + userDTO.getLogin());
        System.out.println("E-mail: " + userDTO.getEmail());
        System.out.println("FIRST NAME: " + userDTO.getFirstName());
        System.out.println("LAST NAME: " + userDTO.getLastName());
        System.out.println("MIDDLE NAME: " + userDTO.getMiddleName());
        System.out.println("[OK]");
    }
}
