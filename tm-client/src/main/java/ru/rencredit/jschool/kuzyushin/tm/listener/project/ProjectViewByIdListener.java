package ru.rencredit.jschool.kuzyushin.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.rencredit.jschool.kuzyushin.tm.api.ISessionService;
import ru.rencredit.jschool.kuzyushin.tm.event.ConsoleEvent;
import ru.rencredit.jschool.kuzyushin.tm.listener.AbstractListener;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

@Component
public final class ProjectViewByIdListener extends AbstractListener {

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @Autowired
    public ProjectViewByIdListener(
            final @NotNull ProjectEndpoint projectEndpoint,
            final @NotNull ISessionService sessionService
    ) {
        super(sessionService);
        this.projectEndpoint = projectEndpoint;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id";
    }

    @Override
    @EventListener(condition = "@projectViewByIdListener.name() == #event.command")
    public void handler(final ConsoleEvent event) {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final SessionDTO sessionDTO = sessionService.getCurrentSession();
        @Nullable final ProjectDTO projectDTO = projectEndpoint.findProjectById(sessionDTO, id);
        if (projectDTO == null) return;
        System.out.println("ID: " + projectDTO.getId());
        System.out.println("NAME: " + projectDTO.getName());
        System.out.println("DESCRIPTION: " + projectDTO.getDescription());
        System.out.println("[OK]");
    }
}
